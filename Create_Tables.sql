DROP TABLE IF EXISTS CountyComments;
DROP TABLE IF EXISTS StateComments;
DROP TABLE IF EXISTS CountyElectionPartyVote;
DROP TABLE IF EXISTS CountyElection;
DROP TABLE IF EXISTS CountyCensus;
DROP TABLE IF EXISTS County;
DROP TABLE IF EXISTS TempCounty;
DROP TABLE IF EXISTS Poll;
DROP TABLE IF EXISTS StateElectionPartyVote;
DROP TABLE IF EXISTS StateElection;
DROP TABLE IF EXISTS StateCensus;
DROP TABLE IF EXISTS State;
DROP TABLE IF EXISTS Party;

CREATE TABLE State(
	stateID TINYINT(2) NOT NULL,
	stateName VARCHAR(14) NOT NULL,
	stateAbbreviation CHAR(2) NOT NULL,
	PRIMARY KEY (stateID)
);
CREATE UNIQUE INDEX abbr ON State (stateAbbreviation);

CREATE TABLE StateCensus(
	stateID TINYINT(2) NOT NULL,
	censusYear SMALLINT NOT NULL CHECK (MOD(censusYear,10)=0),
	Population INT,
	VotingAgeCitizens INT,
	Men INT,
	Women INT,
	White DECIMAL(5,2) CHECK (0.00<=White AND White<=100.00),
	Asian DECIMAL(5,2) CHECK (0.00<=Asian AND Asian<=100.00),
	Black DECIMAL(5,2) CHECK (0.00<=Black AND Black<=100.00),
	Native DECIMAL(5,2) CHECK (0.00<=Native AND Native<=100.00),
	Pacific DECIMAL(5,2) CHECK (0.00<=Pacific AND Pacific<=100.00),
	Hispanic DECIMAL(5,2) CHECK (0.00<=Hispanic AND Hispanic<=100.00),
	IncomePerCapita INT,
	EmployedWorkers INT,
	Unemployment DECIMAL(5,2) CHECK (0.00<=Unemployment AND Unemployment<=100.00),
	Poverty DECIMAL(5,2) CHECK (0.00<=Poverty AND Poverty<=100.00),
	ChildPoverty DECIMAL(5,2) CHECK (0.00<=ChildPoverty AND ChildPoverty<=100.00),
	PrivateWork DECIMAL(5,2) CHECK (0.00<=PrivateWork AND PrivateWork<=100.00),
  PublicWork DECIMAL(5,2) CHECK (0.00<=PublicWork AND PublicWork<=100.00),
	SelfEmployedWork DECIMAL(5,2) CHECK (0.00<=SelfEmployedWork AND SelfEmployedWork<=100.00),
	FamilyWorkers DECIMAL(5,2) CHECK (0.00<=FamilyWorkers AND FamilyWorkers<=100.00),
	Professional DECIMAL(5,2) CHECK (0.00<=Professional AND Professional<=100.00),
	Service DECIMAL(5,2) CHECK (0.00<=Service AND Service<=100.00),
	Construction DECIMAL(5,2) CHECK (0.00<=Construction AND Construction<=100.00),
	Production DECIMAL(5,2) CHECK (0.00<=Production AND Production<=100.00),
	Office DECIMAL(5,2) CHECK (0.00<=Office AND Office<=100.00),
	WorkAtHome DECIMAL(5,2) CHECK (0.00<=WorkAtHome AND WorkAtHome<=100.00),
	Commute DECIMAL(5,2) CHECK (0.00<=Commute AND Commute<=100.00),
	Drive DECIMAL(5,2) CHECK (0.00<=Drive AND Drive<=100.00),
	CarPool DECIMAL(5,2) CHECK (0.00<=CarPool AND CarPool<=100.00),
	Transit DECIMAL(5,2) CHECK (0.00<=Transit AND Transit<=100.00),
	Walk DECIMAL(5,2) CHECK (0.00<=Walk AND Walk<=100.00),
	COVIDCases INT,
	COVIDDeaths INT,
	PRIMARY KEY (stateID, censusYear),
	FOREIGN KEY (stateID) REFERENCES State(stateID)
);
--CREATE INDEX stateCensusYear ON State (censusYear);

CREATE TABLE Party(
	partyID TINYINT(1) NOT NULL,
	partyName VARCHAR(20) NOT NULL,
	partyAbbreviation CHAR(3) NOT NULL,
	PRIMARY KEY (partyID)
);

CREATE TABLE Poll(
	pollID MEDIUMINT NOT NULL,
	questionID MEDIUMINT NOT NULL,
	Cycle SMALLINT NOT NULL CHECK (Cycle IN (2016, 2020)),
	stateID TINYINT(2),
	StartDay DATE,
	EndDay DATE,
	SampleSize INT,
	RepublicanPercent DECIMAL(5,2) CHECK (0.00<=RepublicanPercent AND RepublicanPercent<=100.00),
	DemocratPercent DECIMAL(5,2)
	CHECK (0.00<=DemocratPercent AND DemocratPercent <= 100.00),
	PRIMARY KEY (Cycle, pollID, questionID),
	FOREIGN KEY (stateID) REFERENCES State(stateID)
);

CREATE TABLE StateElection(
	stateID TINYINT(2) NOT NULL,
	Cycle SMALLINT NOT NULL CHECK (MOD(Cycle,4)=0),
	TotalVote INT,
	WinningParty TINYINT(1),
	AbsoluteMargin INT,
	PercentMargin DECIMAL(5,2),
	PRIMARY KEY (stateID, Cycle),
	FOREIGN KEY (stateID) REFERENCES State(stateID),
	FOREIGN KEY (WinningParty) REFERENCES Party(partyID)
);


CREATE TABLE StateElectionPartyVote(
	stateID TINYINT(2) NOT NULL,
	Cycle SMALLINT NOT NULL CHECK (MOD(Cycle,4)=0),
	partyID TINYINT(1) NOT NULL,
	Vote INT NOT NULL,
	VotePercent DECIMAL(5,2),
	PRIMARY KEY (stateID, Cycle, partyID),
	FOREIGN KEY (stateID, Cycle) REFERENCES StateElection(stateID, Cycle),
	FOREIGN KEY (partyID) REFERENCES Party(partyID)
);

CREATE TABLE County(
	stateID TINYINT(2) NOT NULL,
	countyID SMALLINT NOT NULL,
	countyName VARCHAR(50) NOT NULL,
	Latitude DECIMAL(20,17),
	Longnitude DECIMAL(20,17),
	PRIMARY KEY (countyID),
	FOREIGN KEY (stateID) REFERENCES State(stateID)
);

CREATE TABLE CountyCensus(
	countyID SMALLINT NOT NULL,
	censusYear SMALLINT NOT NULL CHECK (MOD(censusYear,10)=0),
	Population INT,
	VotingAgeCitizens INT,
	Men INT,
	Women INT,
	White DECIMAL(5,2) CHECK (0.00<=White AND White<=100.00),
	Asian DECIMAL(5,2) CHECK (0.00<=Asian AND Asian<=100.00),
	Black DECIMAL(5,2) CHECK (0.00<=Black AND Black<=100.00),
	Native DECIMAL(5,2) CHECK (0.00<=Native AND Native<=100.00),
	Pacific DECIMAL(5,2) CHECK (0.00<=Pacific AND Pacific<=100.00),
	Hispanic DECIMAL(5,2) CHECK (0.00<=Hispanic AND Hispanic<=100.00),
	IncomePerCapita INT,
	EmployedWorkers INT,
	Unemployment DECIMAL(5,2) CHECK (0.00<=Unemployment AND Unemployment<=100.00),
	Poverty DECIMAL(5,2) CHECK (0.00<=Poverty AND Poverty<=100.00),
	ChildPoverty DECIMAL(5,2) CHECK (0.00<=ChildPoverty AND ChildPoverty<=100.00),
	PrivateWork DECIMAL(5,2) CHECK (0.00<=PrivateWork AND PrivateWork<=100.00),
	PublicWork DECIMAL(5,2) CHECK (0.00<=PublicWork AND PublicWork<=100.00),
	SelfEmployedWork DECIMAL(5,2) CHECK (0.00<=SelfEmployedWork AND SelfEmployedWork<=100.00),
	FamilyWorkers DECIMAL(5,2) CHECK (0.00<=FamilyWorkers AND FamilyWorkers<=100.00),
	Professional DECIMAL(5,2) CHECK (0.00<=Professional AND Professional<=100.00),
	Service DECIMAL(5,2) CHECK (0.00<=Service AND Service<=100.00),
	Construction DECIMAL(5,2) CHECK (0.00<=Construction AND Construction<=100.00),
	Production DECIMAL(5,2) CHECK (0.00<=Production AND Production<=100.00),
	Office DECIMAL(5,2) CHECK (0.00<=Office AND Office<=100.00),
	WorkAtHome DECIMAL(5,2) CHECK (0.00<=WorkAtHome AND WorkAtHome<=100.00),
	Commute DECIMAL(5,2) CHECK (0.00<=Commute AND Commute<=100.00),
	Drive DECIMAL(5,2) CHECK (0.00<=Drive AND Drive<=100.00),
	CarPool DECIMAL(5,2) CHECK (0.00<=CarPool AND CarPool<=100.00),
	Transit DECIMAL(5,2) CHECK (0.00<=Transit AND Transit<=100.00),
	Walk DECIMAL(5,2) CHECK (0.00<=Walk AND Walk<=100.00),
	COVIDCases INT,
	COVIDDeaths INT,
	PRIMARY KEY (countyID, censusYear),
	FOREIGN KEY (countyID) REFERENCES County(countyID)
);
--CREATE INDEX countyCensusYear ON State (censusYear);

CREATE TABLE CountyElection(
	countyID SMALLINT NOT NULL,
	Cycle SMALLINT NOT NULL CHECK (MOD(Cycle,4)=0),
	TotalVote INT,
	WinningParty TINYINT(1),
	AbsoluteMargin INT,
	PercentMargin DECIMAL(5,2),
	PRIMARY KEY (countyID, Cycle),
	FOREIGN KEY (countyID) REFERENCES County(countyID),
	FOREIGN KEY (WinningParty) REFERENCES Party(partyID)
);

CREATE TABLE CountyElectionPartyVote(
	countyID SMALLINT NOT NULL,
	Cycle SMALLINT NOT NULL CHECK (MOD(Cycle,4)=0),
	partyID TINYINT(1) NOT NULL,
	Vote INT NOT NULL,
	VotePercent DECIMAL(5,2),
	PRIMARY KEY (countyID, Cycle, partyID),
	FOREIGN KEY (countyID, Cycle) REFERENCES CountyElection(countyID, Cycle),
	FOREIGN KEY (partyID) REFERENCES Party(partyID)
);

CREATE TABLE StateComments(
  stateID TINYINT(2) NOT NULL,
  comment VARCHAR(200),
  PRIMARY KEY (stateID),
  FOREIGN KEY (stateID) REFERENCES State(stateID)
);

CREATE TABLE CountyComments(
  countyID SMALLINT NOT NULL,
  stateID TINYINT(2) NOT NULL,
  comment VARCHAR(200),
  PRIMARY KEY (countyID),
  FOREIGN KEY (countyID) REFERENCES County(countyID),
  FOREIGN KEY (stateID) REFERENCES State(stateID)
);
