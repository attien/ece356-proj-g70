DELETE FROM StateElectionPartyVote;
DELETE FROM StateElection;

INSERT INTO StateElection (
		stateID, 
		Cycle
	)
	SELECT
		State.stateID,
		2016
	FROM State;
	
INSERT INTO StateElection (
		stateID, 
		Cycle
	)
	SELECT
		State.stateID,
		2020
	FROM State;
 
 

INSERT INTO StateElectionPartyVote 
	SELECT stateID, Cycle, partyID, SumVote, SumVote/TotalVote 
	FROM (
		SELECT stateID, 
        Cycle, 
        partyID, 
        SUM(Vote) AS SumVote, 
        SUM(Vote/VotePercent) AS TotalVote 
		FROM (CountyElectionPartyVote INNER JOIN County ON County.countyID = CountyElectionPartyVote.countyID)
		GROUP BY stateID, Cycle, partyID
	) AS CData;
	
UPDATE StateElection
	SET
    TotalVote = (SELECT SUM(TotalVote) FROM CountyElection WHERE countyID IN (SELECT countyID FROM County WHERE stateID = StateElection.stateID) AND Cycle = StateElection.Cycle),
		WinningParty = (SELECT partyID FROM StateElectionPartyVote WHERE stateID = StateElection.stateID AND Cycle = StateElection.Cycle ORDER BY Vote DESC LIMIT 1),
		AbsoluteMargin = (SELECT Vote FROM StateElectionPartyVote WHERE stateID = StateElection.stateID AND Cycle = StateElection.Cycle ORDER BY Vote DESC LIMIT 1)-(SELECT Vote FROM StateElectionPartyVote WHERE stateID = StateElection.stateID AND Cycle = StateElection.Cycle ORDER BY Vote DESC LIMIT 1, 1),
		PercentMargin = (SELECT VotePercent FROM StateElectionPartyVote WHERE stateID = StateElection.stateID AND Cycle = StateElection.Cycle ORDER BY VotePercent DESC LIMIT 1)-(SELECT VotePercent FROM StateElectionPartyVote WHERE stateID = StateElection.stateID AND Cycle = StateElection.Cycle ORDER BY VotePercent DESC LIMIT 1, 1);
