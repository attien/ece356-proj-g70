import mysql.connector
from mysql.connector import errorcode
from Globals import *

class DbConnector: 
    def __init__(self, username, password, host, db_name): 
        self.username = username
        self.password = password
        self.host = host 
        self.DB_NAME = db_name
        cnx = mysql.connector.connect(user=self.username, password=self.password, host=self.host)
        try:
            cursor = cnx.cursor()
            cursor.execute("USE {}".format(self.DB_NAME))
            cursor.close()
            cnx.close()
        except mysql.connector.Error as err:
            print("Error: {}".format(err))
            exit(1)

    def setup_tables_and_db(self, create_db: bool): 
        if create_db: 
            self.__create_db()
        self.__create_and_populate_tables()
    
    # Create database if it does not already exist, use it 
    def __create_db(self): 
        try: 
            self.cnx = mysql.connector.connect(user=self.username, password=self.password)
            cursor = self.cnx.cursor()
            cursor.execute("CREATE DATABASE IF NOT EXISTS {};".format(self.DB_NAME))
            print("Successfully created database '{}'".format(self.DB_NAME))
            self.cnx.commit()
            cursor.close()
            self.cnx.close()
        except mysql.connector.Error as err:
            print("Failed creating database: {}".format(err))
            exit(1)

    def __create_and_populate_tables(self):
        print("Creating tables...")
        f = open(CREATE_TABLES_SQL_FILE, 'r')
        create_contents = f.read().split(";\n")
        for stmt in create_contents:
            stmt = stmt.strip()
            stmt = self.__noncomment_part(stmt)
            if not stmt: # empty query / string case
                continue
            cnx = self.connect_to_db()
            cursor = cnx.cursor()
            cursor.execute(stmt)
            cnx.commit()
            cursor.close()
            cnx.close()
        print("Successfully created tables. Beginning population...")

        for file in POPULATE_TABLES_SQL_FILE_LIST: 
            # print(file)
            print("Executing " + file)
            with open(file, 'r') as f: 
                contents = f.read().split(";\n") 
                for stmt in contents: 
                    stmt = stmt.strip() 
                    stmt = self.__noncomment_part(stmt)
                    if not stmt: # empty query / string case
                        continue
                    cnx = self.connect_to_db()
                    cursor = cnx.cursor()
                    # print(stmt)
                    cursor.execute(stmt)
                    cnx.commit()
                    cursor.close()
                    cnx.close()
            print("Successfully populated table using " + file)
        print("Successfully finished populating all tables.")

    # parse SQL file if there happens to be comments 
    def __noncomment_part(self, stmt):
        if stmt.startswith("/*"): 
            if len(stmt.split("*/")) > 1: 
                stmt = stmt.split("*/")[-1]
            else: 
                stmt = ""
        elif stmt.startswith("*/"): 
            stmt = stmt.split("*/")[-1]
        elif stmt.startswith("--"): 
            stmt = "\n".join(stmt.split("\n")[1:])
        return stmt

    # this can be used AFTER create_db has been called for the first time
    def connect_to_db(self):
        cnx = mysql.connector.connect(user=self.username, password=self.password, host=self.host)
        try:
            cursor = cnx.cursor()
            cursor.execute("USE {}".format(self.DB_NAME))
            cursor.close()
        except mysql.connector.Error as err:
            print("Error: {}".format(err))
            exit(1)
        return cnx

        # cnx2 = self.connect_to_db()
        # cursor = cnx2.cursor()
        # cursor.execute("INSERT INTO State (stateID, stateName, stateAbbreviation) VALUES (1, \"Alabama\", \"AL\")"); 
        # cnx2.commit()
        # cursor.close()
        # cnx2.close()

# input_username = input("Enter your MySql username: ")
# input_pass = input("Enter your MySql password: ")
# test = DbConnector(username=input_username, password=input_pass)
# test.create_db(); 
