DROP TABLE IF EXISTS TrumpBidenPoll;
DROP TABLE IF EXISTS TrumpClintonPoll;

CREATE TABLE TrumpBidenPoll(
	pollID INT,
	questionID INT,
	state VARCHAR(20),
	startDay VARCHAR(8),
	endDay VARCHAR(8),
	sampleSize INT,
	candidateParty CHAR(3),
	percent DECIMAL(3,1)
);


LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/trump_biden_polls.csv' INTO TABLE TrumpBidenPoll
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(
    questionID, pollID, @dummy, state, @dummy, @dummy, 
	@dummy, @dummy, @dummy, @dummy, @dummy, @dummy,
    @sampleSize, @dummy, @dummy, @dummy, @dummy, @dummy, 
	@dummy, startDay, endDay, @dummy, @dummy, @dummy, 
	@dummy, @dummy, @dummy, @dummy, @dummy, @dummy, 
	@dummy, @dummy, @dummy, @dummy, @dummy, @dummy, 
	candidateParty, percent
)
SET sampleSize = NULLIF(@sampleSize, '');


INSERT INTO Poll SELECT
		pollID,
		questionID,
		2020,
		(SELECT stateID FROM State WHERE State.stateName = state),
		STR_TO_DATE(startDay, "%m/%d/%y"),
		STR_TO_DATE(endDay, "%m/%d/%y"),
		sampleSize,
		repPercent,
		demPercent FROM (SELECT REP.pollID as pollID,
		REP.questionID as questionID,
		REP.state as state,
		REP.startDay as startDay,
		REP.endDay as endDay,
		REP.sampleSize as sampleSize,
		REP.percent as repPercent,
		DEM.percent as demPercent
	FROM (SELECT * FROM TrumpBidenPoll WHERE candidateParty = "REP") AS REP
	INNER JOIN (SELECT * FROM TrumpBidenPoll WHERE candidateParty = "DEM") AS DEM
	ON DEM.pollID = REP.pollID AND DEM.questionID = REP.questionID) AS DemRep;

CREATE TABLE TrumpClintonPoll(
	pollID INT,
	state VARCHAR(20),
	startDay VARCHAR(10),
	endDay VARCHAR(10),
	sampleSize INT,
	clinton DECIMAL(17,15),
  trump DECIMAL(17,15)
);

LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/trump_clinton_polls.csv' INTO TABLE TrumpClintonPoll
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(
  pollID, startDay, endDay, state, @dummy, @dummy, @sampleSize, @dummy, clinton, trump, @dummy
)
SET sampleSize = NULLIF(@sampleSize, '');

INSERT INTO Poll SELECT
		pollID,
		1,
		2016,
		(SELECT stateID FROM State WHERE State.stateName = state),
		startDay,
		endDay,
		sampleSize,
		trump, 
		clinton
	FROM TrumpClintonPoll;
