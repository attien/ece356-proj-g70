DROP TABLE IF EXISTS TempCounty;

CREATE TABLE TempCounty(
	countyID SMALLINT NOT NULL,
	countyName VARCHAR(50) NOT NULL,
	stateAbbreviation CHAR(2) NOT NULL,
	2016TrumpVotePercent DECIMAL(5,2),
	2016HillaryVotePercent DECIMAL(5,2),
	2016TotalVote INT,
	2016TrumpVote INT,
	2016HillaryVote INT,
	2020TrumpVotePercent DECIMAL(5,2),
	2020BidenVotePercent DECIMAL(5,2),
	2020TotalVote INT,
	2020TrumpVote INT,
	2020BidenVote INT,
	Latitude DECIMAL(20,17),
	Longnitude DECIMAL(20,17),
	COVIDCases INT,
	COVIDDeaths INT,
	Population INT,
	Men INT,
	Women INT,
	Hispanic DECIMAL(5,2) CHECK (0.00<=Hispanic AND Hispanic<=100.00),
	White DECIMAL(5,2) CHECK (0.00<=White AND White<=100.00),
	Black DECIMAL(5,2) CHECK (0.00<=Black AND Black<=100.00),
	Asian DECIMAL(5,2) CHECK (0.00<=Asian AND Asian<=100.00),
	Native DECIMAL(5,2) CHECK (0.00<=Native AND Native<=100.00),
	Pacific DECIMAL(5,2) CHECK (0.00<=Pacific AND Pacific<=100.00),
	VotingAgeCitizens INT,
	IncomePerCapita INT,
	Poverty DECIMAL(5,2) CHECK (0.00<=Poverty AND Poverty<=100.00),
	ChildPoverty DECIMAL(5,2) CHECK (0.00<=ChildPoverty AND ChildPoverty<=100.00),
	Professional DECIMAL(5,2) CHECK (0.00<=Professional AND Professional<=100.00),
	Service DECIMAL(5,2) CHECK (0.00<=Service AND Service<=100.00),
	Office DECIMAL(5,2) CHECK (0.00<=Office AND Office<=100.00),
	Construction DECIMAL(5,2) CHECK (0.00<=Construction AND Construction<=100.00),
	Production DECIMAL(5,2) CHECK (0.00<=Production AND Production<=100.00),
	Drive DECIMAL(5,2) CHECK (0.00<=Drive AND Drive<=100.00),
	CarPool DECIMAL(5,2) CHECK (0.00<=CarPool AND CarPool<=100.00),
	Transit DECIMAL(5,2) CHECK (0.00<=Transit AND Transit<=100.00),
	Walk DECIMAL(5,2) CHECK (0.00<=Walk AND Walk<=100.00),
	WorkAtHome DECIMAL(5,2) CHECK (0.00<=WorkAtHome AND WorkAtHome<=100.00),
	Commute DECIMAL(5,2) CHECK (0.00<=Commute AND Commute<=100.00),
	EmployedWorkers INT,
	PrivateWork DECIMAL(5,2) CHECK (0.00<=PrivateWork AND PrivateWork<=100.00),
	PublicWork DECIMAL(5,2) CHECK (0.00<=PublicWork AND PublicWork<=100.00),
	SelfEmployedWork DECIMAL(5,2) CHECK (0.00<=SelfEmployedWork AND SelfEmployedWork<=100.00),
	FamilyWorkers DECIMAL(5,2) CHECK (0.00<=FamilyWorkers AND FamilyWorkers<=100.00),
	Unemployment DECIMAL(5,2) CHECK (0.00<=Unemployment AND Unemployment<=100.00),
	PRIMARY KEY (countyID),
	FOREIGN KEY (stateAbbreviation) REFERENCES State(stateAbbreviation)
);

LOAD DATA INFILE '/var/lib/mysql-files/05-2020-Election/county_statistics.csv' INTO TABLE TempCounty
FIELDS TERMINATED BY ',' 
LINES TERMINATED BY '\n'
IGNORE 1 LINES
(
	countyID, countyName, stateAbbreviation, 
	@2016TrumpVotePercent, @2016HillaryVotePercent, @2016TotalVote, @2016TrumpVote, @2016HillaryVote, 
	@2020TrumpVotePercent, @2020BidenVotePercent, @2020TotalVote, @2020TrumpVote, @2020BidenVote, 
	@Latitude, @Longnitude, @COVIDCases, @COVIDDeaths,
	@Population, @Men, @Women, @Hispanic, @White, @Black, @Native, @Asian, @Pacific, 
	@VotingAgeCitizens,
	@dummy, @dummy, @IncomePerCapita, @dummy, @Poverty, @ChildPoverty,
	@Professional, @Service, @Office, @Construction, @Production,
	@Drive, @CarPool, @Transit, @Walk, @dummy, @WorkAtHome, @Commute, 
	@EmployedWorkers, @PrivateWork, @PublicWork, @SelfEmployedWork, @FamilyWorkers, @Unemployment
)
SET
	2016TrumpVotePercent = NULLIF(@2016TrumpVotePercent,''),
	2016HillaryVotePercent = NULLIF(@2016HillaryVotePercent,''),
	2016TotalVote = NULLIF(@2016TotalVote,''),
	2016TrumpVote = NULLIF(@2016TrumpVote,''),
	2016HillaryVote = NULLIF(@2016HillaryVote,''), 
	2020TrumpVotePercent = NULLIF(@2020TrumpVotePercent,''),
	2020BidenVotePercent = NULLIF(@2020BidenVotePercent,''),
	2020TotalVote = NULLIF(@2020TotalVote,''),
	2020TrumpVote = NULLIF(@2020TrumpVote,''),
	2020BidenVote = NULLIF(@2020BidenVote,''),
	Latitude = NULLIF(@Latitude,''),
	Longnitude = NULLIF(@Longnitude,''),
	COVIDCases = NULLIF(@COVIDCases,''),
	COVIDDeaths = NULLIF(@COVIDDeaths,''),
	Population = NULLIF(@Population,''),
	Men = NULLIF(@Men,''),
	Women = NULLIF(@Women,''),
	Hispanic = NULLIF(@Hispanic,''),
	White = NULLIF(@White,''),
	Black = NULLIF(@Black,''),
	Native = NULLIF(@Native,''),
	Asian = NULLIF(@Asian,''),
	Pacific = NULLIF(@Pacific,''), 
	VotingAgeCitizens = NULLIF(@VotingAgeCitizens,''),
	IncomePerCapita = NULLIF(@IncomePerCapita,''),
	Poverty = NULLIF(@Poverty,''),
	ChildPoverty = NULLIF(@ChildPoverty,''),
	Professional = NULLIF(@Professional,''),
	Service = NULLIF(@Service,''),
	Office = NULLIF(@Office,''),
	Construction = NULLIF(@Construction,''),
	Production = NULLIF(@Production,''),
	Drive = NULLIF(@Drive,''),
	CarPool = NULLIF(@CarPool,''),
	Transit = NULLIF(@Transit,''),
	Walk = NULLIF(@Walk,''),
	WorkAtHome = NULLIF(@WorkAtHome,''),
	Commute = NULLIF(@Commute,''), 
	EmployedWorkers = NULLIF(@EmployedWorkers,''),
	PrivateWork = NULLIF(@PrivateWork,''),
	PublicWork = NULLIF(@PublicWork,''),
	SelfEmployedWork = NULLIF(@SelfEmployedWork,''),
	FamilyWorkers = NULLIF(@FamilyWorkers,''),
	Unemployment = NULLIF(@Unemployment,'');

-- Clear old data to prevent duplicate errors 
DELETE FROM CountyElectionPartyVote;
DELETE FROM CountyElection;
DELETE FROM CountyCensus;
DELETE FROM County;

-- Populate all of County
INSERT INTO County (
		stateID,
		countyID,
		countyName,
		Latitude,
		Longnitude
	)
	SELECT
		(SELECT stateID FROM State WHERE State.stateAbbreviation = TempCounty.stateAbbreviation),
		TempCounty.countyID,
		TempCounty.countyName,
		TempCounty.Latitude,
		TempCounty.Longnitude
	FROM TempCounty;

/*
	Populate CountyElection
	For the 2016 and 2020 Cycles
	To allow populating CountyElectionPartyVote
*/
INSERT INTO CountyElection (
		countyID,
		Cycle,
		TotalVote
	)
	SELECT
		TempCounty.countyID,
		2016,
		TempCounty.2016TotalVote
	FROM TempCounty;
INSERT INTO CountyElection (
		countyID,
		Cycle,
		TotalVote
	)
	SELECT
		TempCounty.countyID,
		2020,
		TempCounty.2020TotalVote
	FROM TempCounty;

/*
	Fully populate CountyElectionPartyVote
	For the Republican and Democrat Parties
	For the 2016 and 2020 Cycles
*/
INSERT INTO CountyElectionPartyVote (
		countyID,
		Cycle,
		partyID,
		Vote,
		VotePercent
	)
	SELECT
		TempCounty.countyID,
		2016,
		(SELECT partyID FROM Party WHERE partyAbbreviation = 'REP'),
		TempCounty.2016TrumpVote,
		100*TempCounty.2016TrumpVotePercent
	FROM TempCounty WHERE 2016TrumpVote IS NOT NULL;

INSERT INTO CountyElectionPartyVote (
		countyID,
		Cycle,
		partyID,
		Vote,
		VotePercent
	)
	SELECT
		TempCounty.countyID,
		2016,
		(SELECT partyID FROM Party WHERE partyAbbreviation = 'DEM'),
		TempCounty.2016HillaryVote,
		100*TempCounty.2016HillaryVotePercent
	FROM TempCounty WHERE 2016HillaryVote IS NOT NULL;

INSERT INTO CountyElectionPartyVote (
		countyID,
		Cycle,
		partyID,
		Vote,
		VotePercent
	)
	SELECT
		TempCounty.countyID,
		2020,
		(SELECT partyID FROM Party WHERE partyAbbreviation = 'REP'),
		TempCounty.2020TrumpVote,
		100*TempCounty.2020TrumpVotePercent
	FROM TempCounty WHERE 2020TrumpVote IS NOT NULL;

INSERT INTO CountyElectionPartyVote (
		countyID,
		Cycle,
		partyID,
		Vote,
		VotePercent
	)
	SELECT
		TempCounty.countyID,
		2020,
		(SELECT partyID FROM Party WHERE partyAbbreviation = 'DEM'),
		TempCounty.2020BidenVote,
		100*TempCounty.2020BidenVotePercent
	FROM TempCounty WHERE 2020BidenVote IS NOT NULL;
	
/*
	Update CountyElection using CountyElectionPartyVote
	
		WinningParty = ( SELECT partyID FROM CountyElectionPartyVote WHERE 
			countyID = CountyElection.countyID 
			AND Cycle = CountyElection.Cycle 
			AND Vote = ( SELECT MAX(Vote) FROM CountyElectionPartyVote WHERE 
				countyID = CountyElection.countyID AND Cycle = CountyElection.Cycle 
			)
		)
		WITH CEPV AS
		SELECT () - () FROM (SELECT Vote FROM CountyElectionPartyVote WHERE countyID = 1 AND Cycle = 2016 ORDER BY Vote DESC);
*/
UPDATE CountyElection
	SET
		WinningParty = (SELECT partyID FROM CountyElectionPartyVote WHERE countyID = CountyElection.countyID AND Cycle = CountyElection.Cycle ORDER BY Vote DESC LIMIT 1),
		AbsoluteMargin = (SELECT Vote FROM CountyElectionPartyVote WHERE countyID = CountyElection.countyID AND Cycle = CountyElection.Cycle ORDER BY Vote DESC LIMIT 1)-(SELECT Vote FROM CountyElectionPartyVote WHERE countyID = CountyElection.countyID AND Cycle = CountyElection.Cycle ORDER BY Vote DESC LIMIT 1, 1),
		PercentMargin = (SELECT VotePercent FROM CountyElectionPartyVote WHERE countyID = CountyElection.countyID AND Cycle = CountyElection.Cycle ORDER BY VotePercent DESC LIMIT 1)-(SELECT VotePercent FROM CountyElectionPartyVote WHERE countyID = CountyElection.countyID AND Cycle = CountyElection.Cycle ORDER BY VotePercent DESC LIMIT 1, 1);

-- Fully populate CountyCensus
INSERT INTO CountyCensus (
		countyID,
		censusYear,
		Population,
		VotingAgeCitizens,
		Men,
		Women,
		White,
		Asian,
		Black,
		Native,
		Pacific,
		Hispanic,
		IncomePerCapita,
		EmployedWorkers,
		Unemployment,
		Poverty,
		ChildPoverty,
		PrivateWork,
		PublicWork,
		SelfEmployedWork,
		FamilyWorkers,
		Professional,
		Service,
		Construction,
		Production,
		Office,
		WorkAtHome,
		Commute,
		Drive,
		CarPool,
		Transit,
		Walk,
		COVIDCases,
		COVIDDeaths
	)
	SELECT 
		TempCounty.countyID,
		2010,
		TempCounty.Population,
		TempCounty.VotingAgeCitizens,
		TempCounty.Men,
		TempCounty.Women,
		TempCounty.White,
		TempCounty.Asian,
		TempCounty.Black,
		TempCounty.Native,
		TempCounty.Pacific,
		TempCounty.Hispanic,
		TempCounty.IncomePerCapita,
		TempCounty.EmployedWorkers,
		TempCounty.Unemployment,
		TempCounty.Poverty,
		TempCounty.ChildPoverty,
		TempCounty.PrivateWork,
		TempCounty.PublicWork,
		TempCounty.SelfEmployedWork,
		TempCounty.FamilyWorkers,
		TempCounty.Professional,
		TempCounty.Service,
		TempCounty.Construction,
		TempCounty.Production,
		TempCounty.Office,
		TempCounty.WorkAtHome,
		TempCounty.Commute,
		TempCounty.Drive,
		TempCounty.CarPool,
		TempCounty.Transit,
		TempCounty.Walk,
		TempCounty.COVIDCases,
		TempCounty.COVIDDeaths
	FROM TempCounty;
	
DROP TABLE IF EXISTS TempCounty;

INSERT INTO CountyComments (countyID, stateID) SELECT countyID, stateID FROM County;