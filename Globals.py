# TESTING_ON_MARMOSET = True
CREATE_TABLES_SQL_FILE = "Create_Tables.sql"
POPULATE_TABLES_SQL_FILE_LIST = [
    "Populate_States.sql",
    "Populate_Parties.sql",
    "Populate_Counties.sql",
    "Populate_StateCensus.sql",
    "Populate_StateElections.sql",
    "Populate_Polls.sql"
]

PARTY = {1 : "Republican", 2 : "Democrat"}
DEMOGRAPHY = {1 : "Population", 2: "VotingAgeCitizens", 3: "Men", 4: "Women", 5: "White", 6: "Asian", 7 : "Black", 8: "Native", 9 : "Pacific", 10: "Hispanic", 11: "IncomePerCapita", 12: "EmployedWorkers", 13: "PrivateWork", 14: "PublicWork", 15: "SelfEmployed", 16: "FamilyWork", 17: "Unemployed", 18: "Poverty", 19: "ChildPoverty", 20: "Professional", 21: "Service", 22: "Office", 23: "Construction", 24: "Production", 25: "Drive", 26: "Carpool", 27: "Transit", 28: "Walk", 29: "WorkAtHome", 30: "Commute", 31: "COVIDCases", 32: "COVIDDeaths", }