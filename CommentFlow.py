from DbConnector import DbConnector

INVALID_STATE_COUNTY_INPUT = "Invalid input. Press \"q\" to exit this flow. Otherwise, please indicate your choice by selecting s/c: "
STATE_NOT_FOUND = "The input state could not be found. If you want to quit out of this flow, press \"q\". Otherwise, please check your spelling and re-enter: "
COUNTY_NOT_FOUND = "The input county for the corresponding state could not be found. If you want to quit out of this flow, press \"q\". Otherwise, please check your spelling and re-enter: "
COMMENT_ADDED = "Comment added. You can view this comment by selecting the \"v\" option if you continue with the flow.\n\n"
COMMENT_LIMIT_EXCEEDED = "The comment has exceeded 200 characters. Please enter a comment with valid length (limit 200 characters): "
TOTAL_COMMENT_LIMIT_EXCEEDED = "The total comment length has exceeded 200 characters. If you still wish to add this comment, please keep it under {} characters: "

state = {}
county = {}
countyIN = {} #for constant lookup time in both directions 
stateIN = {}

def stateEditor(set):
  while True:
    user_proceed = input("Please choose the states and counties you want to search. Your selection will be saved between queries. If left empty, the default is all states\n" +
  		"1. Add a state or county \n" +
    	"2. Check all selected states and counties\n" +
    	"3. Clear all\n"
    	"4. Proceed\n")
    if (user_proceed == "1"):
      addState(set, state, county)
    elif (user_proceed == "2"):
      if not set: 
        print("No current selections.\n")
      else:
        for stateNum in set:
          print(stateIN[stateNum], "-", len(set[stateNum]) , "counties selected")
    elif (user_proceed == "3"):
      set.clear()
    elif (user_proceed == "4"):
      return
    else:
      continue
  
def addState(set, state, county):
  stateIn = input("What state do you want to add?\n")
  
  if stateIn in state:
    if state[stateIn] not in set:
      set[state[stateIn]] = []
    num = 1
    print("List of counties: ")
    for countySet in county[state[stateIn]]:
      print(num, ". ", countySet[0])
      num = num + 1
    choice = input("Input a for all counties, n for no counties, or have a list separated by spaces (such as 1 3 5 6 7 etc)\n")
    if choice == "n":
      pass
    elif choice == "a":
      for countySet in county[state[stateIn]]:
        set[state[stateIn]].append(countySet[1])
    else:
      arr = choice.split()
      for num in arr:
        number = county[state[stateIn]][int(num)-1][1]
        set[state[stateIn]].append(number)
  else:
    print("Sorry, state not found. Try again")

def comment_flow(db_connector_obj: DbConnector):
    print("You have entered the commenting / annotation flow for the Elections database. Here you can make " +
        "new comments about a particular state or county, add onto an existing comment, or view any comments you've already made " +
        "about any or all states / counties.\n"
    )
    cnx = db_connector_obj.connect_to_db()
    cursor = cnx.cursor()
    query = "SELECT stateName, stateID FROM State;"
    cursor.execute(query)
    for stateName, stateID in cursor:
        state[stateName] = stateID
        stateIN[stateID] = stateName
        county[stateID] = []
    query = "SELECT stateID, countyID, countyName from County ORDER BY stateID;"
    cursor.execute(query)
    for stateID, countyID, countyName in cursor:
        county[stateID].append([countyName, countyID])
        countyIN[countyID] = countyName

    while True: 
        user_proceed = input("1. Add new comment or overwrite existing comment\n" +
            "2. Add to existing comment\n" + 
            "3. View comments\n" + 
            "q. Return to client starting screen\n")
        if user_proceed == "1" or user_proceed == "2": 
            if user_proceed == "1":
              new_comment = True
            else:
              new_comment = False
            while True: 
                chosen_state = input("Please enter the state you wish to comment on: ")
                if chosen_state not in state:
                    print(STATE_NOT_FOUND)
                    continue
                num = 1
                print("List of counties: ")
                for countySet in county[state[chosen_state]]:
                  print(num, ". ", countySet[0])
                  num = num + 1
                choice = input("Input 0 to comment on state, or choose a county (select the corresponding number): ")      
                    
                if choice.lower() == "0":
                    add_comment_by_state(db_connector_obj, chosen_state, state[chosen_state], is_new_comment=new_comment)
                    print(COMMENT_ADDED)                        
                    break
                else: 
                    add_comment_by_county(db_connector_obj, chosen_state, county[state[chosen_state]][int(choice)-1][0], county[state[chosen_state]][int(choice)-1][1], is_new_comment=new_comment)
                    print(COMMENT_ADDED)
                    break

        elif user_proceed.lower() == "3": 
            set = {}
            stateEditor(set)
            view_comments(db_connector_obj, set)



        elif user_proceed.lower() == "q": 
            print("Returning to starting screen...")
            return 
        else: 
            user_proceed = print("Invalid input - please try again.")

def view_comments(db_connector_obj: DbConnector, set):
    if not set:
        query = "SELECT stateName, comment FROM StateComments AS sc INNER JOIN State AS st ON sc.stateID = st.stateID WHERE comment IS NOT NULL ORDER BY sc.stateID"
        cnx = db_connector_obj.connect_to_db()
        cursor = cnx.cursor()
        cursor.execute(query)
        if (cursor.rowcount == 0): 
            print("No existing comments made on states.")
        else: 
            for (stateName, comment) in cursor: 
                print("\n\nState: {}\nComment: {}\n\n".format(stateName,comment))
        cursor.close()
        cnx.close()
    else:
      for state in set:
        if not set[state]:
            query = "SELECT stateID, comment FROM StateComments WHERE stateID = " + str(state) + " AND comment IS NOT NULL;"
            cnx = db_connector_obj.connect_to_db()
            cursor = cnx.cursor()
            cursor.execute(query)
            if cursor.rowcount == 0: 
                print("\nNo results found for selected set.\n")
                return
            for (stateID, comment) in cursor:
                try:
                    print("\n\nState: {}\nComment: {}\n\n".format(stateIN[stateID],comment))
                except:
                    pass
            cursor.close()
            cnx.close()
        else:
            t = tuple(set[state])
            if len(t) == 1:
                t = str(tuple(t)).replace(","," ")
            query = """SELECT stateID, countyID, comment FROM CountyComments 
                WHERE comment IS NOT NULL 
                AND countyID IN {}
                ORDER BY countyID""".format(t)
            cnx = db_connector_obj.connect_to_db()
            cursor = cnx.cursor()
            cursor.execute(query)
            if cursor.rowcount == 0: 
                print("\nNo results found for selected set.\n")
                return
            for (stateID, countyID, comment) in cursor: 
                try:
                    print("State: {}\nCounty: {}\nComment: {}\n\n".format(stateIN[stateID], countyIN[countyID], comment))
                except:
                    pass
            cursor.close()
            cnx.close()

# make a comment -> new comment (overwrites if comment already exists.) or modify (add onto comment using CONCAT())
# countyID is a unique identifier, don't need to query by both stateID and countyID 
def add_comment_by_county(db_connector_obj: DbConnector, state_name, county_name, countyID, is_new_comment): 
    if (is_new_comment): 
        county_comment = input(
            "Please enter the comment you would like to make about {} county of {} state (limit 200 characters): ".format(county_name, state_name)
        )
        while len(county_comment) > 200: 
            county_comment = input(COMMENT_LIMIT_EXCEEDED)
        update_county_comments_table(db_connector_obj, county_comment, countyID)
    else: 
        # this means we are adding onto an existing comment 
        county_addon_comment = input(
            "Please enter the comment you would like to add onto the existing comments for {} county of {} state: ".format(county_name, state_name)
        )
        query = "SELECT comment FROM CountyComments WHERE countyID = {}".format(countyID)
        cnx = db_connector_obj.connect_to_db()
        cursor = cnx.cursor()
        cursor.execute(query)
        for comment in cursor.fetchone(): 
            cursor.close()
            cnx.close()
            if comment: 
                if len(comment) == 200: # previous comment already at limit 
                    print("The comment field has already reached its character limit. You can choose to overwrite the existing comment back at the original comment flow screen.")
                    return
                elif len(county_addon_comment) > 200-len(comment): 
                    while len(county_addon_comment) > 200-len(comment): 
                        county_addon_comment = input(TOTAL_COMMENT_LIMIT_EXCEEDED.format(200-len(comment)))
                        if (county_addon_comment) == "q": 
                            break
                    concat_county_comments_table(db_connector_obj, county_addon_comment, countyID)
                else: 
                    concat_county_comments_table(db_connector_obj, county_addon_comment, countyID)
                print(COMMENT_ADDED)
            else: 
                while len(county_addon_comment) > 200: 
                    county_addon_comment = input(COMMENT_LIMIT_EXCEEDED)
                    if county_addon_comment == "q": 
                        return 
                update_county_comments_table(db_connector_obj, county_addon_comment, countyID)
        

def add_comment_by_state(db_connector_obj: DbConnector, state_name, stateID, is_new_comment): 
    if (is_new_comment):
        state_comment = input(
            "Please enter the comment you would like to make about {} state (limit 200 characters): ".format(state_name)
        )
        while len(state_comment) > 200: 
            state_comment = input(COMMENT_LIMIT_EXCEEDED)

        update_state_comments_table(db_connector_obj, state_comment, stateID)
    else: 
        # this means we are adding onto an existing comment 
        state_addon_comment = input(
            "Please enter the comment you would like to add onto existing {} state comments: ".format(state_name)
        ) 
        # need to do 200-(length of current existing string. this is the limit )
        query = "SELECT comment FROM StateComments WHERE stateID = {}".format(stateID)
        cnx = db_connector_obj.connect_to_db()
        cursor = cnx.cursor()
        cursor.execute(query)
        for row in cursor:
            original_comment = row[0]
            cursor.close()
            cnx.close()
            if original_comment: # found a previous comment 
                if len(original_comment) == 200: # the previous comment has already hit the field limit, so we tell the user to go back and choose the new comment option if they want to overwrite it 
                    print("The comment field has already reached its character limit. You can choose to overwrite the existing comment back at the original comment flow screen.")
                    return 
                elif len(state_addon_comment) > 200-len(original_comment): 
                    while len(state_addon_comment) > 200-len(original_comment): 
                        state_addon_comment = input(TOTAL_COMMENT_LIMIT_EXCEEDED.format(200-len(original_comment)))
                        if state_addon_comment.lower() == "q":
                            break 
                    concat_state_comments_table(db_connector_obj, state_addon_comment, stateID)
                else: 
                    concat_state_comments_table(db_connector_obj, state_addon_comment, stateID)
                print(COMMENT_ADDED)
            else: # there was no previous comment
                while len(state_addon_comment) > 200: 
                    state_addon_comment = input(COMMENT_LIMIT_EXCEEDED)
                    if state_addon_comment == "q": 
                        return
                update_state_comments_table(db_connector_obj, state_addon_comment, stateID)

def update_state_comments_table(db_connector_obj: DbConnector, comment, stateID):
    query = """UPDATE StateComments 
        SET comment = "{}"
        WHERE stateID = {}""".format(comment, stateID)
    cnx = db_connector_obj.connect_to_db() 
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()

def concat_state_comments_table(db_connector_obj: DbConnector, addon_comment, stateID): 
    query =  """UPDATE StateComments 
        SET comment = CONCAT(comment, " {}")
        WHERE stateID = {}""".format(addon_comment, stateID)
    cnx = db_connector_obj.connect_to_db()
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()

def update_county_comments_table(db_connector_obj: DbConnector, comment, countyID): 
    query = """UPDATE CountyComments
        SET comment = "{}"
        WHERE countyID = {}""".format(comment, countyID)
    cnx = db_connector_obj.connect_to_db() 
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()

def concat_county_comments_table(db_connector_obj: DbConnector, comment, countyID): 
    query = """UPDATE CountyComments
        SET comment = CONCAT(comment, " {}")
        WHERE countyID = {}""".format(comment, countyID)
    cnx = db_connector_obj.connect_to_db() 
    cursor = cnx.cursor()
    cursor.execute(query)
    cnx.commit()
    cursor.close()
    cnx.close()
