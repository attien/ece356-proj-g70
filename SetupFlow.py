from DbConnector import DbConnector
from Globals import *
from getpass import getpass

# on marmoset / eceubuntu: python3 SetupFlow.py
user_proceed = input("Welcome to the set-up flow for the 2020 elections voting database!" +
    " This flow involves creating a database and tables related to voting counts by county," + 
    " state, and nationwide along with other relevant factors such as demographic." +
    " If you have already gone through this setup flow, following through will" +
    " delete and re-create everything. Do you wish to proceed?\n" +
    "Select y to proceed with setup, or n to quit the program (y/n): ")

while True: 
    if (user_proceed.lower() == "y"): 
        print("Proceeding with set-up!")
        input_host = input("Enter the hostname (e.g. localhost, marmoset04.shoshin.uwaterloo.ca): ")
        input_username = input("Enter your database server username: ")
        input_pass = getpass("Enter your database server password: ")

        while True: 
            create_db_or_not = input("\nYou can choose to create a database or use an existing database to hold the election data (tables will be created in the existing db).\n" +
                "1. Create a database\n" +
                "2. Choose an existing database to hold election data (e.g. db356_<userid>): \n" +
                "3. Quit\n"
            )
            if create_db_or_not == "1": 
                print("A database named 'election_data' will be created.")
                start_connection = DbConnector(username=input_username, password=input_pass, host=input_host, db_name='election_data')
                final_db_name = 'election_data'
                start_connection.setup_tables_and_db(create_db=True)
                break
            elif create_db_or_not == "2": 
                existing_db = input("Please enter the name of the existing database you wish to use: ")
                start_connection = DbConnector(username=input_username, password=input_pass, host=input_host, db_name=existing_db)
                final_db_name = existing_db
                start_connection.setup_tables_and_db(create_db=False)
                break
            elif create_db_or_not == "3": 
                print("Exiting...")
                exit()
            else: 
                print("Invalid input - please look at the options and try again.")

        print("\nSet-up is complete. You can run queries and look for information in the '{}' database".format(final_db_name))
        break
    elif (user_proceed.lower() == "n"): 
        print("Exiting...")
        exit()
    else:
        user_proceed = input("Invalid input - please indicate your choice by selecting y/n: ")

# Welcome user, ask initial categories to look at 
# Categories: County level, State level 