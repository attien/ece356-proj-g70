from Globals import *
from DbConnector import DbConnector
from QueryFlow import *
from CommentFlow import *
from getpass import getpass


# on marmoset / eceubuntu: python3 CentralFlow.py
user_proceed  = input("\n\nWelcome to the 2020 Elections database user client. \n" +
    "\nThe purpose of this interface is to help your political campaign find areas to target next \n" + 
    "by identifying strengths and weaknesses based on voting results from the 2020 election. \n" +
	"\nWould you like to get started? (y/n): ")

while True: 
    if (user_proceed == "y"):
        db_name = input("Please enter the name of the database that the election data is in (if you do not have a database setup, please run SetupFlow.py): ")
        print("\nPlease enter the host and your database credentials below.")
        input_host = input("Enter the hostname (e.g. localhost, marmoset04.shoshin.uwaterloo.ca): ")
        input_username = input("Enter your database server username: ")
        input_pass = getpass("Enter your database server password: ")
        start_connection = DbConnector(username=input_username, password=input_pass, host=input_host, db_name=db_name)
        break
    elif (user_proceed == "n"): 
        print("\nThe client is now exiting...\n\n")
        exit()
    else: 
        user_proceed = input("Invalid input - please indicate your initial choice by selecting y/n: ")
    
 

while True: 
    user_proceed_to_query_mod = input("\n\nThe client supports searching for different voting statistics based on location, and demographic. \n" + 
    "This includes looking at topics such as electoral margin-of-victory between political parties by county and state. \n" + 
    "As well as voting tendencies based on socioeconomic status and race. \n"+
	"You can also look at topics such as bellweather counties, voting margin shifts between the 2016 and 2020 election, and more.\n\n"
    "It also supports making comments or annotations on specific counties/states that you have particular interest in investigating further. \n\n" +
	"Would you like to search for data or make a comment? \n" +
    "You can also enter \"q\" to exit the flow when prompted that you can do so. \n" +
	"Otherwise, please enter \"s\" for data search, and \"c\" for comment (s/c): ")
 
    if (user_proceed_to_query_mod.lower() == "s"): 
        # call QueryFlow via. functions or class. pass in start_connection as the DbConnector object 
        enter_queryflow(start_connection)
        continue
    elif (user_proceed_to_query_mod.lower() == "c"): 
        # call ModificationFlow, pass in start_connection as the DbConnector object
        # call for a user_proceed_to_query_mod input again later? if the user wants to switch to the other mode 
        # or continue making queries 
        comment_flow(db_connector_obj=start_connection)
        continue
    else: 
        user_proceed_to_query_mod = print("Invalid input - please indicate your choice by selecting s/c: ")
