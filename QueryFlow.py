from DbConnector import DbConnector
from Globals import *
import mysql.connector

state = {}
county = {}
countyIN = {} #for constant lookup time in both directions 
stateIN = {}

def enter_queryflow(start_connection):
  cnx = start_connection.connect_to_db()
  cursor = cnx.cursor()
  
  query = "SELECT stateName, stateID FROM State;"
  cursor.execute(query)
  for stateName, stateID in cursor:
    state[stateName] = stateID
    stateIN[stateID] = stateName
    county[stateID] = []
  query = "SELECT stateID, countyID, countyName from County ORDER BY stateID;"
  cursor.execute(query)
  for stateID, countyID, countyName in cursor:
    county[stateID].append([countyName, countyID])
    countyIN[countyID] = countyName
  
  set = {}
  while True:
    print("\n\nWelcome to the search service. Please choose one of the queries below:")
    user_choice = input(
      "1. Get election results\n" +
    	"2. Get election swing\n" +
      "3. Get demographic data\n" +
      "4. Get poll results \n" + 
      "5. Get accuracy of polls\n" +
      "6. States by margin\n" +
      "q. Return to menu\n")
    if user_choice == "1":
      getElectionResults(cursor, set)
    elif user_choice == "2":
      getElectionSwing(cursor, set)
    elif user_choice == "3": 
      votesByDemography(cursor, set)
    elif user_choice == "4":
      getPolls(cursor, set)
    elif user_choice == "5":
      pollAccuracy(cursor, set)
    elif user_choice == "6":
      voteShift(cursor, set)
    elif user_choice == "q":
      return
    else:
      print("Invalid choice")
      continue
      
def stateEditor(set):
  while True:
    user_proceed = input("Please choose the states and counties you want to search. Your selection will be saved between queries. If left empty, the default is all states\n" +
  		"1. Add a state or county \n" +
    	"2. Check all selected states and counties\n" +
    	"3. Clear all\n"
    	"4. Proceed\n")
    if (user_proceed == "1"):
      addState(set, state, county)
    elif (user_proceed == "2"):
      for stateNum in set:
        print(stateIN[stateNum], "-", len(set[stateNum]) , "counties selected")
    elif (user_proceed == "3"):
      set.clear()
    elif (user_proceed == "4"):
      return
    else:
      print("Choose valid option")
      continue
  
def addState(set, state, county):
  
  while True:
    stateIn = input("What state do you want to add?\n")
    if stateIn in state:
      if state[stateIn] not in set:
        set[state[stateIn]] = []
      num = 1
      for countySet in county[state[stateIn]]:
        print(num, ". ", countySet[0])
        num = num + 1
      choice = input("Input a for all counties, n for no counties, or have a list separated by spaces (such as 1 3 5 6 7 etc)\n")
      if choice == "n":
        pass
      elif choice == "a":
        for countySet in county[state[stateIn]]:
          set[state[stateIn]].append(countySet[1])
      else:
        arr = choice.split()
        for num in arr:
          number = county[state[stateIn]][int(num)-1][1]
          set[state[stateIn]].append(number)
      return
    else:
      print("Sorry, state not found. Try again")
      continue

def getElectionResults(cursor, set):
  stateEditor(set)
  while True:
    year = input("Select a year (2016 or 2020): ")
    if year != "2016" and year != "2020":
      print("Invalid year")
      continue
    else:
      break
  
  print("{:20s} {:20s} {:20s} {:20s} {:20s}".format("Name", "Total votes", "Winning party", "Absolute margin", "Percent margin"))
  if not set:
    print("====================================================================================================")
    query = "SELECT stateID, TotalVote, WinningParty, AbsoluteMargin, PercentMargin FROM StateElection WHERE Cycle = " + str(year)
    cursor.execute(query)
    for stateID, TotalVote, WinningParty, AbsoluteMargin, PercentMargin in cursor:
      try:
        print("{:20s} {:<20d} {:20s} {:<20d} {:<20.2f}".format(stateIN[stateID], TotalVote, PARTY[WinningParty], AbsoluteMargin, PercentMargin))
      except:
        pass
  else:
    for stateNum in set:
      query = "SELECT stateID, TotalVote, WinningParty, AbsoluteMargin, PercentMargin FROM StateElection WHERE Cycle = " + str(year) + " AND stateID = " + str(stateNum)
      cursor.execute(query)
      print("====================================================================================================")
      for stateID, TotalVote, WinningParty, AbsoluteMargin, PercentMargin in cursor:
        try:
          print("{:20s} {:<20d} {:20s} {:<20d} {:<20.2f}".format(stateIN[stateID], TotalVote, PARTY[WinningParty], AbsoluteMargin, PercentMargin))
        except:
          pass
      print("====================================================================================================")
      for countyNum in set[stateNum]:
        query = "SELECT countyID, TotalVote, WinningParty, AbsoluteMargin, PercentMargin FROM CountyElection WHERE Cycle = " + str(year) + " AND countyID = " + str(countyNum)
        cursor.execute(query)
        for countyID, cTotalVote, cWinningParty, cAbsoluteMargin, cPercentMargin in cursor:
          try:
            print("{:20s} {:<20d} {:20s} {:<20d} {:<20.2f}".format(countyIN[countyID], cTotalVote, PARTY[cWinningParty], cAbsoluteMargin, cPercentMargin))
          except:
            pass

def getElectionSwing(cursor, set):
  stateEditor(set)
  print("{:20s} {:20s} {:20s} {:20s}".format("Name", "Total votes", "Party change", "Percent swing"))
  if not set:
    print("================================================================================")
    query = "SELECT One.stateID, Two.TotalVote, One.WinningParty, Two.WinningParty, One.PercentMargin, Two.PercentMargin FROM (SELECT * FROM StateElection WHERE Cycle = 2016) AS One INNER JOIN (SELECT * FROM StateElection WHERE Cycle = 2020) AS Two ON One.stateID = Two.stateID;"
    cursor.execute(query)
    for stateID, vote2, party1, party2, margin1, margin2 in cursor:
      try:
        if party1 == party2:
          print("{:20s} {:<20d} {:20s} {:<20.4f}".format(stateIN[stateID], vote2, PARTY[party2], margin2 - margin1))
        else:
          print("{:20s} {:<20d} {:20s} {:<20.4f}".format(stateIN[stateID], vote2, PARTY[party2] + " => CHANGED", margin2 + margin1))
      except:
        pass
  else:
    for stateNum in set:
      query = "SELECT One.stateID, Two.TotalVote, One.WinningParty, Two.WinningParty, One.PercentMargin, Two.PercentMargin FROM (SELECT * FROM StateElection WHERE Cycle = 2016 AND stateID = " + str(stateNum) + ") AS One INNER JOIN (SELECT * FROM StateElection WHERE Cycle = 2020 AND stateID = " + str(stateNum) + ") AS Two;"
      cursor.execute(query)
      print("================================================================================")
      for stateID, vote2, party1, party2, margin1, margin2 in cursor:
        try:
          if party1 == party2:
            print("{:20s} {:<20d} {:20s} {:<20.2f}".format(stateIN[stateID], vote2, PARTY[party2], margin2 - margin1))
          else:
            print("{:20s} {:<20d} {:20s} {:<20.2f}".format(stateIN[stateID], vote2, PARTY[party2] + " => CHANGED", margin2 + margin1))
        except:
          pass
      print("================================================================================")
      for countyNum in set[stateNum]:
        query = "SELECT One.countyID, Two.TotalVote, One.WinningParty, Two.WinningParty, One.PercentMargin, Two.PercentMargin FROM (SELECT * FROM CountyElection WHERE Cycle = 2016 AND countyID = " + str(countyNum) + ") AS One INNER JOIN (SELECT * FROM CountyElection WHERE Cycle = 2020 AND countyID = " + str(countyNum) + ") AS Two;"
        cursor.execute(query)
        for countyID, cvote2, cparty1, cparty2, cmargin1, cmargin2 in cursor:
          try:
            if cparty1 == cparty2:
              print("{:20s} {:<20d} {:20s} {:<20.2f}".format(countyIN[countyID], cvote2, PARTY[cparty2], cmargin2 - cmargin1))
            else:
              print("{:20s} {:<20d} {:20s} {:<20.2f}".format(countyIN[countyID], cvote2, PARTY[cparty2] + " => CHANGED", cmargin2 + cmargin1))
          except:
            pass
 
            
def votesByDemography(cursor, set):
  while True:
    choice = input("1. Rank demography by state\n" + 
      "2. Rank demography by county\n" +
      "3. Demographic breakdown of selected areas\n")
    if choice == "1" or choice == "2" or choice == "3":
      break
    else:
      print("Invalid choice")
      continue
  
  if choice == "1":
    for key, value in DEMOGRAPHY.items():
      print(key, value)
    attr = int(input("Select an attribute to see votes: "))
    while True:
      year = input("Select a year (2016 or 2020): ")
      if year != "2016" and year != "2020":
        print("Invalid year")
        continue
      else: 
        break
    print("{:20s} {:20s} {:20s} {:20s} {:20s}".format("State", DEMOGRAPHY[attr], "Party", "Absolute margin", "Percent margin"))
    print("====================================================================================================")
    query = "SELECT ELE.stateID, StateCensus." + DEMOGRAPHY[attr] + ", ELE.WinningParty, ELE.AbsoluteMargin, ELE.PercentMargin FROM (SELECT * FROM StateElection WHERE Cycle = " + year + ") AS ELE INNER JOIN StateCensus On ELE.stateID = StateCensus.stateID ORDER BY " + DEMOGRAPHY[attr] + ";"
    cursor.execute(query)
    for stateID, choice, partynum, absmargin, permargin in cursor:
      try:
        print("{:20s} {:<20.2f} {:20s} {:<20d} {:<20.2f}".format(stateIN[stateID], choice, PARTY[partynum], absmargin, permargin))
      except:
        pass
  elif choice == "2":
    stateChoice = input("What state? ")
    if stateChoice not in state:
      print("Invalid state")
      return
    for key, value in DEMOGRAPHY.items():
      print(key, value)
    attr = int(input("Select an attribute to see votes: "))
    while True:
      year = input("Select a year (2016 or 2020): ")
      if year != "2016" and year != "2020":
        print("Invalid year")
        continue
      else: 
        break
    print("{:20s} {:20s} {:20s} {:20s} {:20s}".format("County", DEMOGRAPHY[attr], "Party", "Absolute margin", "Percent margin"))
    print("====================================================================================================")
    query = "SELECT CountyCensus.countyID, " + DEMOGRAPHY[attr] + ", WinningParty, AbsoluteMargin, PercentMargin FROM (SELECT * FROM (SELECT countyID from County WHERE stateID = " + str(state[stateChoice]) + ") AS county NATURAL JOIN (SELECT * FROM CountyElection WHERE Cycle = " + year + ") AS ele) AS count INNER JOIN CountyCensus on count.countyID = CountyCensus.countyID ORDER BY " + DEMOGRAPHY[attr] + ";"
    cursor.execute(query)
    for countyID, choice, partynum, absmargin, permargin in cursor:
      try:
        print("{:20s} {:<20.2f} {:20s} {:<20d} {:<20.2f}".format(countyIN[countyID], choice, PARTY[partynum], absmargin, permargin))
      except:
        pass
  elif choice == "3":
    stateEditor(set)
    for key, value in DEMOGRAPHY.items():
      print(key, value)
    attr = input("Select attributes for your selected locations as a list (such as 1 4 6 8): ")
    arr = attr.split()
    attributes = ""
    line = "===================="
    formatted_output = "{:20s} ".format("Name")
    for ele in arr:
      line = line + "===================="
      attributes =  attributes +  ","  + DEMOGRAPHY[int(ele)]
      formatted_output = formatted_output + "{:20s} ".format(DEMOGRAPHY[int(ele)])
    print(formatted_output)
    print (line)
    if not set:
      query = "SELECT stateID" + attributes + " FROM StateCensus;"
      cursor.execute(query)
      for itemlist in cursor:
        try:
          formatted_out = "{:20s} ".format(stateIN[itemlist[0]])
          for item in itemlist[1:]:
            formatted_out = formatted_out + "{:<20.2f} ".format(item)
          print(formatted_out)
        except:
          pass
    else:
      for stateNum in set:
        query = "SELECT stateID" + attributes + " FROM StateCensus WHERE stateID = " + str(stateNum) + ";"
        cursor.execute(query)
        for itemlist in cursor:
          try:
            formatted_out = "{:20s} ".format(stateIN[itemlist[0]])
            formatted_line = "===================="
            for item in itemlist[1:]:
              formatted_out = formatted_out + "{:<20.2f} ".format(item)
              formatted_line = formatted_line + "===================="
            print(formatted_line)
            print(formatted_out)
            print(formatted_line)
          except:
            pass
        for countyNum in set[stateNum]:
          query = "SELECT countyID" + attributes + " FROM CountyCensus WHERE countyID = " + str(countyNum) + ";"
          cursor.execute(query)
          for itemlist in cursor:
            try:
              formatted_out = "{:20s} ".format(countyIN[itemlist[0]])
              for item in itemlist[1:]:
                formatted_out = formatted_out + "{:<20.2f} ".format(item)
              print(formatted_out)
            except:
              pass
            
def pollAccuracy(cursor, set):
  while True:
    year = input("Select a year to analyze poll (2016 or 2020): ")
    if year != "2016" and year != "2020":
      print("Invalid year")
      continue
    else: 
      break
  query = "SELECT POLL.stateID, SampleSize, REP_PER, DEM_PER, REP_REAL, DEM_REAL FROM (SELECT stateID, SampleSize, REP/SampleSize*100 AS REP_PER, DEM/SampleSize*100 AS DEM_PER FROM (SELECT stateID, SUM(SampleSize) AS SampleSize, SUM(SampleSize*RepublicanPercent/100) AS REP, SUM(SampleSize*DemocratPercent/100) AS DEM FROM Poll WHERE Cycle = " + year + " GROUP BY stateID) AS TOTAL) AS POLL INNER JOIN (SELECT DEM.stateID AS stateID, DEM.VotePercent AS DEM_REAL, REP.VotePercent AS REP_REAL FROM (SELECT stateID, VotePercent FROM StateElectionPartyVote WHERE Cycle = " + year + " AND partyID = 1) AS REP INNER JOIN (SELECT stateID, VotePercent FROM StateElectionPartyVote WHERE Cycle = " + year + " AND partyID = 2) AS DEM ON REP.stateID = DEM.stateID) AS ELECTION ON POLL.stateID = ELECTION.stateID;"
  cursor.execute(query)
  print("{:20s} {:20s} {:20s} {:20s} {:20s}".format("State", "Predicted win", "Democrat change", "Republican change", "Changed outcome"))
  print("====================================================================================================")
  for stateID, sampleSize, REP_PER, DEM_PER, REP_REAL, DEM_REAL in cursor:
    if (REP_PER > DEM_PER and REP_REAL < DEM_REAL) or (REP_PER < DEM_PER and REP_REAL > DEM_REAL):
      change = "== YES =="
    else:
      change = "NO"
    if REP_PER > DEM_PER:
      victor = "Republican"
    else:
      victor = "Democrat"
    try:
      print("{:20s} {:20s} {:<20.4f} {:<20.4f} {:20s}".format(stateIN[stateID], victor, DEM_REAL-DEM_PER, REP_REAL-REP_PER, change))
    except:
      pass
    
			
def voteShift(cursor, set):
  stateEditor(set)
  absper = input("1. Use absolute margin\n" +
    "2. Use percent margin\n")
  
    
  if not set:
    print("{:20s} {:20s} {:20s} {:20s}".format("State", "Winning party", "Absolute margin", "Percent margin"))
    print("================================================================================")
    query = "SELECT stateID, WinningParty, AbsoluteMargin, PercentMargin FROM StateElection WHERE WinningParty =1 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" DESC;"
    cursor.execute(query)
    for stateID, WinningParty, AbsoluteMargin, PercentMargin in cursor:
      try:
        print("{:20s} {:20s} {:<20d} {:<20.2f}".format(stateIN[stateID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
      except:
        pass
    query = "SELECT stateID, WinningParty, AbsoluteMargin, PercentMargin FROM StateElection WHERE WinningParty =2 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" ASC;"
    cursor.execute(query)
    for stateID, WinningParty, AbsoluteMargin, PercentMargin in cursor:
      try:
        print("{:20s} {:20s} {:<20d} {:<20.2f}".format(stateIN[stateID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
      except:
        pass
  else:
    print("{:20s} {:20s} {:20s} {:20s}".format("Name", "Winning party", "Absolute margin", "Percent margin"))
    state_list = "(1000"
    for state in set:
      state_list =   state_list + "," + str(state) 
    state_list = state_list + ")"
    query = "SELECT stateID, WinningParty, AbsoluteMargin, PercentMargin FROM StateElection WHERE stateID IN " + state_list + " AND WinningParty =1 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" DESC;"
    cursor.execute(query)
    result = []
    for row in cursor:
      result.append(row)
    for stateID, WinningParty, AbsoluteMargin, PercentMargin in result:
      try:
        print("================================================================================")
        print("{:20s} {:20s} {:<20d} {:<20.2f}".format(stateIN[stateID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
        print("================================================================================")
      except:
        pass
      county_list = "(1000000"
      for county in set[stateID]:
        county_list =  county_list + "," + str(county) 
      county_list = county_list + ")"
      query = "SELECT countyID, WinningParty, AbsoluteMargin, PercentMargin FROM CountyElection WHERE countyID IN " + county_list + " AND WinningParty =1 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" DESC;"
      cursor.execute(query)
      for countyID, WinningParty, AbsoluteMargin, PercentMargin in cursor:
        try:
          print("{:20s} {:20s} {:<20d} {:<20.2f}".format(countyIN[countyID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
        except:
          pass
      query = "SELECT countyID, WinningParty, AbsoluteMargin, PercentMargin FROM CountyElection WHERE countyID IN " + county_list + " AND WinningParty =2 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" ASC;"
      cursor.execute(query)
      for countyID, WinningParty, AbsoluteMargin, PercentMargin in cursor:
        try:
          print("{:20s} {:20s} {:<20d} {:<20.2f}".format(countyIN[countyID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
        except:
          pass
      
          
    query = "SELECT stateID, WinningParty, AbsoluteMargin, PercentMargin FROM StateElection WHERE stateID IN " + state_list + " AND WinningParty =2 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" ASC;"
    cursor.execute(query)
    result = cursor
    for stateID, WinningParty, AbsoluteMargin, PercentMargin in result:
      try:
        print("================================================================================")
        print("{:20s} {:20s} {:<20d} {:<20.2f}".format(stateIN[stateID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
        print("================================================================================")
      except:
        pass
      county_list = "(1000000"
      for county in set[stateID]:
        county_list = county_list + ", " + str(county) 
      county_list = county_list + ")"
      query = "SELECT countyID, WinningParty, AbsoluteMargin, PercentMargin FROM CountyElection WHERE countyID IN " + county_list + " AND WinningParty =1 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" DESC;"
      cursor.execute(query)
      for countyID, WinningParty, AbsoluteMargin, PercentMargin in cursor:
        try:
          print("{:20s} {:20s} {:<20d} {:<20.2f}".format(countyIN[countyID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
        except:
          pass
      query = "SELECT countyID, WinningParty, AbsoluteMargin, PercentMargin FROM CountyElection WHERE countyID IN " + county_list + " AND WinningParty =2 AND Cycle = 2020 ORDER BY " +  ("AbsoluteMargin" if absper == "1" else "PercentMargin") +" ASC;"
      cursor.execute(query)
      for countyID, WinningParty, AbsoluteMargin, PercentMargin in cursor:
        try:
          print("{:20s} {:20s} {:<20d} {:<20.2f}".format(countyIN[countyID], PARTY[WinningParty], AbsoluteMargin, PercentMargin))
        except:
          pass
          
def getPolls(cursor, set):
  choice = input("1. Get polls for single state\n" +
    "2. Get polls for selected locations\n")
  if choice == "1":
    stateIn = input("Choose your state: ")
    if stateIn not in state:
      print("Error, not a state")
      return
    year = input("Select a year (2016 or 2020): ")
    query = "SELECT SampleSize, RepublicanPercent, DemocratPercent FROM Poll WHERE stateID = " + str(state[stateIn]) + " AND Cycle = " + year + ";"
    cursor.execute(query)
    print("{:20s} {:20s} {:20s}".format("Sample size", "Republican Percent", "Democrat Percent"))
    print("============================================================")
    for sampleSize, RepublicanPercent, DemocratPercent in cursor:
      try:
        print("{:<20d} {:<20.2f} {:<20.2f}".format(sampleSize, RepublicanPercent, DemocratPercent))
      except:
        pass
    
  elif choice == "2":
    stateEditor(set)
    year = input("Select a year (2016 or 2020): ")
    if not set:
      print("\n\n")
      print("{:20s} {:20s} {:20s}".format("State", "Republican Percent", "Democrat Percent"))
      print("============================================================")
      query = "SELECT stateID, SumRepublicanPercent/COUNT AS RepublicanPercent, SumDemocratPercent/COUNT AS DemocratPercent FROM (SELECT stateID, COUNT(*) AS COUNT, SUM(RepublicanPercent) AS SumRepublicanPercent, SUM(DemocratPercent) AS SumDemocratPercent FROM Poll WHERE Cycle = " + year + " GROUP BY stateID) AS TotalPercent ORDER BY stateID;" 
      cursor.execute(query)
      for stateID, RepublicanPercent, DemocratPercent in cursor:
        try:
          state_name = stateIN[stateID]
          print("{:20s} {:<20.2f} {:<20.2f}".format(state_name, RepublicanPercent, DemocratPercent))
        except:
          pass
    else:
      print("\n\n")
    print("{:20s} {:20s} {:20s}".format("State", "Republican Percent", "Democrat Percent"))
    print("============================================================")
    for state_id in set:
      query = "SELECT SumRepublicanPercent/COUNT AS RepublicanPercent, SumDemocratPercent/COUNT AS DemocratPercent FROM (SELECT COUNT(*) AS COUNT, SUM(RepublicanPercent) AS SumRepublicanPercent, SUM(DemocratPercent) AS SumDemocratPercent FROM Poll WHERE Cycle = " + year + " AND stateID = " + str(state_id) + ") AS TotalPercent;"
      cursor.execute(query)
      for RepublicanPercent, DemocratPercent in cursor:
        try:
          state_name = stateIN[state_id]
          print("{:20s} {:<20.2f} {:<20.2f}".format(state_name, RepublicanPercent, DemocratPercent))
        except:
          pass
        
      
      
    
    
